
var form =document.querySelector('#comment-form');
var textarea =document.querySelector('#comment-textarea');
var commentContainner = document.querySelector('#comment-container');
var inputName =document.querySelector('#name');
var inputMail =document.querySelector('#mail')

function addComment(username, comentario){
    var newComent = `
    <div class="comment">
        <div class="user">
             <i class="fas fa-user"></i>
            <p class="author">${username}</p>
         </div>
         <i class="far fa-star"></i>
         <i class="far fa-star"></i>
         <i class="far fa-star"></i>
            <p class="descripcion">${comentario}</p>
    </div>    
    `

    var comment = document.createElement('div');
    comment.classList.add('comment');
    var user = document.createElement('div');
    user.classList.add('user');
    comment.append(user);
    var name = document.createElement('div');
    name.classList.add('name');
    comment.append(name);

    var i = document.createElement('i');
    i.classList.add('far','fa-star');

  
        commentContainner.innerHTML =newComent + commentContainner.innerHTML;

}

  form.addEventListener("submit",function(evento){  
    evento.preventDefault();
        var comentario = textarea.value;
        console.log(comentario);
        textarea.value = null;
        var name = inputName.value;
        inputName.value = null;
        addComment(name, comentario);
        var mail = inputMail.value;
        inputMail.value = null;
  });


function addEventToStars() {
  // Ir a traer las estrellas
  var stars = document.querySelectorAll('.fa-star');
  
  // Agregar un evento de click a cada estrella
  stars.forEach(function(elemento) {
  
    // Cuando el usuario haga click cambiar la clase de la estrella
    elemento.addEventListener('click', function() {
      // Verificar si esta vacia para llenarla
      if (this.classList.contains('far')) {
        this.classList.replace('far', 'fas');
      } else {
        // Si esta llena, vaciarla
        this.classList.replace('fas', 'far');
      }
    });
  });
}

addEventToStars();
